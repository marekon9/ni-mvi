"""Convert image
Command line tool that converts RGB input image of size 300x200 (width x height).

Expecting three parameters on the input:
generator weights path,
input image,
output file
"""

import tensorflow as tf

from numpy.random import rand
from numpy.random import randn

import numpy as np
from keras.optimizers import Adam
from keras.models import *
from keras.layers import *
from keras_contrib.layers.normalization.instancenormalization import InstanceNormalization
from PIL import Image

def generator_model(input_shape, resnet_blocks=6):
  # Input
  input = Input(shape=input_shape)
  
  # Encoder
  g = Conv2D(filters=64, kernel_size=(7,7), padding='same', kernel_initializer='random_normal')(input)
  g = InstanceNormalization(axis=-1)(g)
  g = ReLU()(g)

  for i in [128, 256]:
    g = Conv2D(filters=i, kernel_size=(3,3), strides=(2,2), padding='same', kernel_initializer='random_normal')(g)
    g = InstanceNormalization(axis=-1)(g)
    g = ReLU()(g)

  # Transformer
  for i in range(resnet_blocks):
    res_in = g
    g = Conv2D(filters=256, kernel_size=(3,3), padding='same', kernel_initializer='random_normal')(g)
    g = InstanceNormalization(axis=-1)(g)
    g = ReLU()(g)
    g = Conv2D(filters=256, kernel_size=(3,3), padding='same', kernel_initializer='random_normal')(g)
    g = InstanceNormalization(axis=-1)(g)
    g = Concatenate()([g, res_in])


  # Decoder
  for i in [128, 64]:
    g = Conv2DTranspose(filters=i, kernel_size=(3,3), strides=(2,2), padding='same', kernel_initializer='random_normal')(g)
    g = InstanceNormalization(axis=-1)(g)
    g = ReLU()(g)

  g = Conv2DTranspose(filters=3, kernel_size=(7,7), padding='same', kernel_initializer='random_normal')(g)
  g = InstanceNormalization(axis=-1)(g)
  output = Activation('tanh')(g)

  # Output
  model = Model(input, output)
  return model

def normalize_image(dataset):
  dataset = dataset.astype('float32')
  dataset = (dataset - 127.5) / 127.5
  return dataset

def denormalize_image(data):
  X = ((data + 1) * 255) / 2
  X = X.astype(np.uint8)
  return Image.fromarray(X)

def transform_image(generator_path, image_path, output_path):
  img = Image.open(image_path)

  data = np.array(img)
  normalize_image(data)

  generator = generator_model((200,300,3))
  generator.load_weights(generator_path)
  generator.compile(optimizer='adam', loss='mse')

  out_data = generator.predict(data)
  out = denormalize_image(out_data)

  out.save(output_path)

if __name__ == "__main__":
  if len(sys.argv) < 4:
    raise Exception("Excepted three input arguments: generator weights, input file, output file")
  transform_image(sys.argv[1], sys.argv[2], sys.argv[3])
