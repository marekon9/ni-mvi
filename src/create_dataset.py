"""
Command line tool for creating numpy dataset from images.

Expecting two parameters on the input:
input folder: folder containing images with name pattern img_{i}.jpg
output file: output .npy file
"""

import numpy as np

from os import listdir
from os.path import isfile, join
from PIL import Image

def save_data_into(source_dir, dest_file):
  print(f'Loading images from {source_dir}')
  cnt = len([f for f in listdir(source_dir) if isfile(join(source_dir, f))])
  dataset = np.array([np.array(Image.open(join(source_dir, f"img_{i}.jpg"))) for i in range(cnt)])
  print(f'Shape is: {dataset.shape}')

  if len(dataset.shape) is not 4:
    raise Exception("The shape of dataset should have 4 dimensions!")
  
  print(f'Saving dataset as {dest_file}')
  np.save(dest_file, dataset)
  print(f'SUCCESS')


if __name__ == "__main__":
  if len(sys.argv) < 3:
    raise Exception("Excepted two input arguments: input folder containing files with pattern img_{i}.jpg and output file (.npy)")
  save_data_into(sys.argv[1], sys.argv[2])

